# Joint Cyber Range distribution of CTFd

## Description

This repo contains code to build a Joint Cyber Range flavored CTFd container image, extending the `ctfd/ctfd` base image with custom CTFd plugins:
    - Initial_setup: Automate the initial CTFd GUI-based setup and more.
    - Container_challenges: Add container-based CTFd challenge type which deploys challenges per CTF player as Kubernetes pods.
    - Custom_auth: Enabling custom SSO (Single Sign-On) authentication using SAML.

## Workflows

### Most basic workflow

- prepare an environment file for the `initial_setup` plugin, e.g. adapt the given one
- `docker run -p 8000:8000 -it --rm --env-file env -t registry.gitlab.com/jointcyberrange.nl/ctfd-docker-with-plugins/development:latest`

Your CTFd will then listen on <http://localhost:8000>.

### Basic workflow

- clone the repo
- make adaptations
- `docker build -t jcr-cftd .`
- prepare an environment file for the `initial_setup` plugin
- `docker run -p 8000:8000 -it --rm --env-file env -t jcr-cftd`

This will show you a local test environment, probably without container challenge support, though you should be able to review the user interface for container challenges.

### Accessing a local Kubernetes cluster workflow (not functional yet)

In this workflow you give access to a cluster without running the CTFd in that cluster. Mac version. Needs fixing.

- `export KUBECONFIG=~/.kube/config`
- test access with `kubectl config get-contexts`
- In CTFd you should not see the message `Failed to initialize Python Kubernetes SDK: Invalid kube-config file. No configuration found.`

### Kubernetes based (developer) workflow

- clone the repo
- login to your favorite docker registry
- make adaptations
- `docker build -t <registry name and image tag> .`
- `docker push <registry name and image tag>`
- prepare an environment file for the `initial_setup` plugin
- create a relevant Kubernetes manifest (coming up real soon)
- `kubectl apply -f manifest.yml`

Note that this is a developer workflow, not a production workflow. You may also want to consider using branches.

We are migrating this to [Argo NG](https://gitlab.com/jointcyberrange.nl/jcr-deploy-by-argo-ng).

### Kubernetes branch based acceptance test and production worklow

- work in a branch
- make adaptions
- commit
- the CI/CD pipeline can create an image in the gitlab repo with an appropriate tag, this typically takes a manual step.
- if needed, adapt the production Kubernetest manifests to refer to this image. For example <https://gitlab.com/jointcyberrange.nl/jcr-deploy-by-argo-cd-do>.
- when using the `latest` tag, kill the pod through `argo` or `kubectl`. Argo will recreate the pod.
- check the image digest at the bottom of a CTFd admin page
- when done, create a merge request

### Workflow for updates of plugins and/or CTFd master

There are a number of levels where we can deviate from the 'latest/main' path. If you do,
your first step is to make a branch of this repo.
The second step:

- in `Dockerfile` adapt tags and sources of plugins; you can also add files to the images.
- commit and watch the pipeline. It may need a manual step to build a new container imagfe.
- do testing with your new container image (more details to be provided), by executing one of the workflows above.

### Deployment of challenges

Deployment of challenges via `ctfcli`: see <https://gitlab.com/jointcyberrange.nl/ctfcli-testchallenges>.

This could potentially fail due to certificate issues, if your target environment does not properly support TLS connections.

## Features

- CTFd plugins
  - Initial_setup
  - Container_challenges
  - Custom_auth
- Container image
- Kubernetes Helm chart
- Tilt

## CTFd plugins

All plugins should have the ability to be used on their own, so open source users could use the initial_setup plugin without using the custom_auth plugin and the other way around `(Note: this is currently not the case)`.

### Initial_setup

The initial_setup CTFd plugin enables the following functionality:
    - Automate the initial CTFd GUI-based setup
      - Set the CTF Event name
      - Set `user` or `teams` mode
      - Create the initial `admin` user
    -  Make advanced settings available through environment variables
       -  Email config for account verification and password reset workflows
       -  Set custom theme (must be pre-loaded using the `Dockerfile`)
    -  Customize the frontpage of the CTFd event using the `index.html.j2` Jinja2 template and a custom logo

#### ToDo Initial setup

- Use Python functions per use case e.g. initial CTFd setup, create Admin user, set index.html + logo, admin panel settings with sub-functions (email, theme)
- Parameterize CTFd admin panel settings such as those already done (email, theme)
- New plugin: allow git clone a theme repository and load in CTFd Web-UI by CTFd event admin
- New plugin: ctfcli embedded as CTFd plugin, provide a list of (private / public) repo's to sync to the CTFd event as ENV vars or config file, make a CTFd Web-UI page so the CTFd event admin can also preview and load challenge repositories using the CTFd Web-UI, could reuse the player challenge overview page as preview page

### Container_challenges

The container_challenges CTFd plugin enables a new CTFd challenge type that allows for challenge infrastructure deployed as Kubernetes pods.

- Container challenge infrastructure is described as `docker-compose.yaml` abstracting the Kubernetes complexity, while allowing you to describe a simple challenge network with multiple containers which can be optionally exposed to the outside.
- Each CTF player will receive an isolated challenge environment (Kubernetes namespace) on the start of a challenge.
- The Kubernetes cluster CTFd is hosted in will also host the container_challenges

#### ToDO container challenges

- Allow for an external Kubernetes cluster to host container challenges by providing a Kubeconfig file
  - Enables the possibility for a dedicated container challenge hosting cluster, either physical or virtual i.e. vCluster
  - This removes the hard dependency for JCR CTFd itself to run in Kubernetes
- Mount flags as files in Kubernetes volumes
- Add CTFd challenge type for container challenges with unique flags by generating flags during the build phase and building inside the image inside the k8s cluster
- Only allow curated method of exposing services i.e. block serviceType LoadBalancer

### Custom_auth

The custom_auth CTFd plugin enables SSO (Single Sign-On) authentication using the SAML protocol for your CTFd Event.

- Use `SETUP_ACCOUNT_TYPE` to define what account type CTFd will use: `ctfd`, `sso` or `both`.

## Container image

### Semantic versioning

We use semantic versioning. Currently this requires manually updating `gitlab-ci.yml` on any code change.

### Container build and run

- clone the repo
- make adaptations
  - Add a plugin or modify a plugin
    - Customize the Event `index.html` Jinja2 template or logo
  - Add a custom theme
- `docker build -t jcr-cftd .`
- prepare an environment file for the `initial_setup` plugin
- `docker run -p 8000:8000 -it --rm --env-file env -t jcr-ctfd:local`
- for a full Kubernetes experience consider using [jcr-argo-ng](https://gitlab.com/jointcyberrange.nl/jcr-deploy-by-argo-ng)

More workflows below.

#### ToDo

- GitLab-CI.yaml which builds container images using buildkit / buildctl
- Push container image as public image to GitLab container registry
- Container image release workflow in GitLab-CI.yaml e.g. branch > MR > test > approve > merge > push > tag > GitLab release
- Markdown badge for container image build pipeline

## Kubernetes Helm chart

The Helm chart included in this repository deploys CTFd in Kubernetes.

The default values for the chart deploys a minimalistic version of CTFd using SQLite as database, this is meant for ephemeral environments such as development and should not be used in production.

- Changes to the Helm chart are primarily made in the templated Kubernetes manifests from the `templates/` directory.
- If a setting is configurable using Helm values, you can include it as a Default value in the file `values.yaml`.
- The MySQL and Redis Bitnami included as subcharts can also be controlled using the `values.yaml` by referencing `mysql` or `redis`, both are disabled by default.
- User instructions after a Helm installation can be provided in the `templates/NOTES.txt`.

### Features

- Parametrize CTFd initial setup plugin using Helm values resulting in a Kubernetes ConfigMap
- Auto generate the CTFd admin user password if none is provided
- Auto generate the CTFd SECRET_KEY
- Load custom CTFd plugins remotely (`git clone`) while using `ctfd/ctfd` as container image
- Optional Bitnami MySQL subchart
- Optional Bitnami Redis subchart

### Usage

#### Helm install

```bash
HELM_CHART_NAME=jcr-ctfd
HELM_CHART_VERSION=0.0.1
HELM_OCI_REGISTRY_ENDPOINT=registry.gitlab.com/
HELM_OCI_REGISTRY_PATH=jointcyberrange.nl/ctfd-docker-with-plugins/helm/
HELM_OCI_REGISTRY_URL="oci://${HELM_OCI_REGISTRY_ENDPOINT}${HELM_OCI_REGISTRY_PATH}${HELM_CHART_NAME}"

helm pull "${HELM_OCI_REGISTRY_URL}" --version "${HELM_CHART_VERSION}"
helm upgrade --install --create-namespace --namespace=jointcyberrange "${HELM_CHART_NAME}" "${HELM_OCI_REGISTRY_URL}"
```

Note: `helm upgrade --install` can be used for both installation and upgrades of Helm charts

#### Helm values

The default values for the Helm chart can be overwritten, as well as the default values of the subcharts.
You can retrieve the default Helm values by running this command if your working directory is this repository.

```bash
helm show values helm_chart
```

You can overwrite values by adding `--values values.yaml` to the `helm upgrade` command.
The following command will create a file called `values.yaml` to overwrite the CTFd container image that the Helm chart will install.

```bash
cat <<EOF | tee $(pwd)/values.yaml
ctfd:
  image: jcr-ctfd:local
EOF
```

#### Remotely load custom CTFd plugins and themes

We offer a custom Joint Cyber Range container image from this repository that includes all our custom CTFd plugins. That said, the Helm chart also allows you to use the official CTFd container image. The following Helm values show you how to define the CTFd container image registry and tag. We include the option to load (git clone and copy) custom CTFd plugins that are not included in the used container image as part of the Kubernetes deployment.

The CTFd container image can be easily extended with the `initial_setup` plugin to automate the initial CTFd Web-based setup and make this setup part of your containerized zero-touch CTFd deployment. It is also possible to load a remote custom CTFd theme using this Helm chart and the `initial_setup` plugin can enable the change the default CTFd theme.

```yaml
ctfd:
  image:
    registry: ctfd/ctfd
    tag: 3.6.1
  setup:
    theme: CTFD-crimson-theme
    custom_index_html:
      path: /opt/CTFd/CTFd/plugins/initial_setup/index.html.j2
      logo: https://static.wikia.nocookie.net/spongebob/images/3/36/Dennis_Stock_Image.png
  load_remote_theme:
    enabled: true
    name: CTFD-crimson-theme
    repo: https://github.com/0xdevsachin/CTFD-crimson-theme
    path: /
  load_remote_plugins:
    enabled: true
    plugins:
      - name: initial_setup
        repo: https://gitlab.com/jointcyberrange.nl/ctfd-docker-with-plugins --branch improvements
        path: /initial_setup
```

#### Ingress controller

The identity provider for the Custom_auth plugin requires an HTTPS connection, to achieve this we use the ingress-nginx ingress controller.

##### Installation

```yaml
helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace
```

##### Helm values for ingress

```bash
cat <<EOF | tee $(pwd)/values.yaml
ctfd:
  ingress:
    enabled: true
    host: kubernetes.docker.internal
EOF
```

##### DNS

If you run this on your local machine using Docker Desktop or an alternative, then you need to add a DNS entry for `kubernetes.docker.internal` in your `/etc/hosts` file.

```bash
IP_ADDRESS=$(kubectl get ingress jcr-ctfd -n jointcyberrange -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
echo "${IP_ADDRESS}    kubernetes.docker.internal" | sudo tee -a /etc/hosts > /dev/null
```

##### Helm uninstall

Cleanup the application:

```bash
helm uninstall --namespace=jointcyberrange jcr-ctfd
```

Cleanup the ingress-nginx controller if installed following this guide:

```bash
helm uninstall --namespace=jointcyberrange jcr-ctfd
```

##### Helm release new chart version

Since Helm v3.8.0 OCI-compliant container registries are natively supported as Helm chart repository.
Therefore we will use the GitLab Container Registry to push our Helm chart to, so the chart can be used remotely by users.

```bash
HELM_CHART_NAME=jcr-ctfd
HELM_CHART_VERSION=0.0.1
HELM_APP_VERSION=3.7.0
HELM_CHART_DIR=./helm_chart
HELM_OCI_REGISTRY_URL=registry.gitlab.com/
HELM_OCI_REGISTRY_PATH=jointcyberrange.nl/ctfd-docker-with-plugins/helm
HELM_OCI_REGISTRY_USERNAME=
HELM_OCI_REGISTRY_PASSWORD=

echo $CI_REGISTRY_PASSWORD | helm registry login -u "${HELM_OCI_REGISTRY_USERNAME}" --password-stdin "${HELM_OCI_REGISTRY_URL}${HELM_OCI_REGISTRY_PATH}"
helm package "${HELM_CHART_DIR}" --dependency-update --version "${HELM_CHART_VERSION}" --app-version "${HELM_APP_VERSION}"
helm push "${HELM_CHART_NAME}-${HELM_CHART_VERSION}.tgz" "oci://${HELM_OCI_REGISTRY_URL}${HELM_OCI_REGISTRY_PATH}"
```

expected output

```bash
Login Succeeded
Successfully packaged chart and saved it to: $pwd/jcr-ctfd-0.0.1.tgz
Pushed: registry.gitlab.com/jointcyberrange.nl/ctfd-docker-with-plugins/helm/jcr-ctfd:0.0.1
Digest: sha256:227552b042d6692a4f5ebf8710de91db67d26b5c716848ab256ccdb1faa59920
```

### Container image

The Helm chart values allow you to define settings for custom CTFd plugins which are not included in the CTFd base image. You will be left out of some features by using the CTFd base image directly in the Helm chart. Use the Joint Cyber Range CTFd image for a full-featured deployment.

### MySQL Database

A MySQL database should be used for production environments, we recommend to use a managed database cloud service or a Kubernetes MySQL database operator.

### ToDO

- Release Helm chart using GitLab-CI.yaml to GitLab package registry as part of a GitLab release
- S3 storage for upload folders (e.g. CloudFlare R2 or Digital Ocean Spaces)
- Allow to insert custom logo and jinja2 template on Kubernetes level (binaryData ConfigMap) instead of including into container image
- Parametrize Kubernetes resource values such as point to existing secrets instead of generate or manual input, allow to define storageClass for persistent storage, allow to overwrite command-line arguments etc.
- Add networkPolicy: default Deny ingress/egress, allow port 8000 from ingress controller, allow egress to in-cluster or external database

## Tilt

Tilt is used for developing the Joint Cyber Range container image inside Kubernetes. When changes are made to either the CTFd plugins, Dockerfile or Helm templates, Tilt will hot-reload the changes.

Tilt uses the Helm chart from this repository to deploy resources to the default Kubernetes namespace, it will port-forward to `localhost:8000` and the CTFd admin user password is always set to `admin` for Tilt deployments.

To use tilt, run:

```bash
tilt up
```

When done with developing, run:

```bash
tilt down
```

Tip: from time to time you'll encounter a change that Tilt can't hot reload and you get stuck, simply run `tilt down` and `tilt up` to reset.

### ToDO

- Allow multiple pre-defined configurations by adding command-line arguments to Tilt
  - Minimal CTFd (SQLite) by default
  - +MySQL
  - +Redis
  - +HTTPS
- Create factory reset button in Tilt to empty database, upload folder, log folders


## Boiler plate

What follows is from the gitlab template, and serves as a reminder and todo list.

## Badges

On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals

Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support

Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

## License

For open source projects, say how it is licensed.

## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
