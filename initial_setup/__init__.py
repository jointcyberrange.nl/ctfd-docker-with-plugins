import CTFd
from CTFd.models import db, Users, Tokens
from CTFd.utils.security.auth import generate_user_token
from CTFd.utils.user import get_current_user, get_current_user_type, is_admin
import sqlalchemy
import secrets
import sys
import datetime
import os
from jinja2 import Environment, FileSystemLoader

def load(app):
    # Check if CTFd needs be to setup
    if not CTFd.utils.config.is_setup():
        # Temporary values
        mustExit = False
        error_prefix = "[ERROR] [" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "] "
        error_suffix = "\n"
        # Retrieve configurations
        SETUP_EVENT_NAME = os.getenv("SETUP_EVENT_NAME")
        SETUP_EVENT_MODE = os.getenv("SETUP_EVENT_MODE")
        SETUP_EVENT_ADMIN_NAME = os.getenv("SETUP_EVENT_ADMIN_NAME")
        SETUP_EVENT_ADMIN_EMAIL = os.getenv("SETUP_EVENT_ADMIN_EMAIL")
        SETUP_EVENT_ADMIN_PASSWORD = os.getenv("SETUP_EVENT_ADMIN_PASSWORD")
        SETUP_ACCOUNT_TYPE = os.getenv("SETUP_ACCOUNT_TYPE")
        SETUP_EMAIL_ENABLED = os.getenv("SETUP_EMAIL_ENABLED")
        EMAIL_SERVER = os.getenv("EMAIL_SERVER")
        EMAIL_PORT = os.getenv("EMAIL_PORT")
        EMAIL_PROTOCOL = os.getenv("EMAIL_PROTOCOL")
        EMAIL_ADDRESS = os.getenv("EMAIL_ADDRESS")
        EMAIL_USERNAME = os.getenv("EMAIL_USERNAME")
        EMAIL_PASSWORD = os.getenv("EMAIL_PASSWORD")
        CUSTOM_INDEX_HTML_PAGE_PATH = os.getenv("CUSTOM_INDEX_HTML_PAGE_PATH", "/opt/CTFd/CTFd/plugins/initial_setup/index.html.j2")
        CUSTOM_INDEX_HTML_PAGE_LOGO_PATH = os.getenv("CUSTOM_INDEX_HTML_PAGE_LOGO_PATH", "themes/core/static/img/logo.png")
        PRELOADED_CTFD_THEME_NAME = os.getenv("PRELOADED_CTFD_THEME_NAME", "core")
        
        # Validate empty setup configurations
        if not SETUP_EVENT_NAME:
            sys.stderr.write(error_prefix + "value of ENV variable \"SETUP_EVENT_NAME\" cannot be empty!" + error_suffix)
            mustExit = True
        if not SETUP_EVENT_MODE:
            sys.stderr.write(error_prefix + "value of ENV variable \"SETUP_EVENT_MODE\" cannot be empty!" + error_suffix)
            mustExit = True
        if not SETUP_EVENT_ADMIN_NAME:
            sys.stderr.write(error_prefix + "value of ENV variable \"SETUP_EVENT_ADMIN_NAME\" cannot be empty!" + error_suffix)
            mustExit = True
        if not SETUP_EVENT_ADMIN_EMAIL:
            sys.stderr.write(error_prefix + "value of ENV variable \"SETUP_EVENT_ADMIN_EMAIL\" cannot be empty!" + error_suffix)
            mustExit = True
        if not SETUP_EVENT_ADMIN_PASSWORD:
            sys.stderr.write(error_prefix + "value of ENV variable \"SETUP_EVENT_ADMIN_PASSWORD\" cannot be empty!" + error_suffix)
            mustExit = True
        if mustExit:
            sys.exit(1)
        # Validate invalid setup configurations
        if SETUP_EVENT_MODE != "users" and SETUP_EVENT_MODE != "teams":
            sys.stderr.write(error_prefix + "value of ENV variable \"SETUP_EVENT_MODE\" cannot be invalid!" + error_suffix)
            mustExit = True
        if not CTFd.utils.validators.validate_email(SETUP_EVENT_ADMIN_EMAIL):
            sys.stderr.write(error_prefix + "value of ENV variable \"SETUP_EVENT_ADMIN_EMAIL\" cannot be invalid!" + error_suffix)
            mustExit = True
        if SETUP_ACCOUNT_TYPE and SETUP_ACCOUNT_TYPE != "ctfd" and SETUP_ACCOUNT_TYPE != "sso" and SETUP_ACCOUNT_TYPE != "both":
            sys.stderr.write(error_prefix + "value of ENV variable \"SETUP_ACCOUNT_TYPE\" cannot be invalid!" + error_suffix)
            mustExit = True
        if SETUP_EMAIL_ENABLED and SETUP_EMAIL_ENABLED != "false" and SETUP_EMAIL_ENABLED != "true":
            sys.stderr.write(error_prefix + "value of ENV variable \"SETUP_EMAIL_ENABLED\" cannot be invalid!" + error_suffix)
            mustExit = True
        if mustExit:
            exit(1)
        # Event name
        CTFd.utils.set_config("ctf_name", SETUP_EVENT_NAME)
        # Event mode; users or teams
        CTFd.utils.set_config("user_mode", SETUP_EVENT_MODE)
        # Register button visibility based on acount type
        if SETUP_ACCOUNT_TYPE == "ctfd":
            CTFd.utils.set_config(CTFd.constants.config.ConfigTypes.REGISTRATION_VISIBILITY, CTFd.constants.config.RegistrationVisibilityTypes.PUBLIC)
        if SETUP_ACCOUNT_TYPE == "sso":
            CTFd.utils.set_config(CTFd.constants.config.ConfigTypes.REGISTRATION_VISIBILITY, CTFd.constants.config.RegistrationVisibilityTypes.PRIVATE)
        if SETUP_ACCOUNT_TYPE == "both":
            CTFd.utils.set_config(CTFd.constants.config.ConfigTypes.REGISTRATION_VISIBILITY, CTFd.constants.config.RegistrationVisibilityTypes.PUBLIC)
        # Other button visibilities
        CTFd.utils.set_config(CTFd.constants.config.ConfigTypes.CHALLENGE_VISIBILITY, CTFd.constants.config.ChallengeVisibilityTypes.PRIVATE)
        CTFd.utils.set_config(CTFd.constants.config.ConfigTypes.SCORE_VISIBILITY, CTFd.constants.config.ScoreVisibilityTypes.PRIVATE)
        CTFd.utils.set_config(CTFd.constants.config.ConfigTypes.ACCOUNT_VISIBILITY, CTFd.constants.config.AccountVisibilityTypes.PRIVATE)
        # Set the theme
        CTFd.utils.set_config("ctf_theme", PRELOADED_CTFD_THEME_NAME)
        # Extract the directory path and filename
        directory_path = os.path.dirname(CUSTOM_INDEX_HTML_PAGE_PATH)
        filename = os.path.basename(CUSTOM_INDEX_HTML_PAGE_PATH)
        # Set up the Jinja environment
        env = Environment(loader=FileSystemLoader(directory_path))
        # Load the template
        template = env.get_template(filename)
        # Render the template with the variables
        indexHTML = template.render(logo_path=CUSTOM_INDEX_HTML_PAGE_LOGO_PATH)
        # Create the page object
        page = CTFd.models.Pages(title=None, route="index", content=indexHTML, draft=False)
        # Add index page to database
        try:
            CTFd.models.db.session.add(page)
            CTFd.models.db.session.commit()
        except sqlalchemy.exc.IntegrityError:
            CTFd.models.db.session.rollback()

        # Create the initial admin user
        user = Users(
            name=os.getenv("SETUP_EVENT_ADMIN_NAME"),
            email=os.getenv("SETUP_EVENT_ADMIN_EMAIL"),
            password=os.getenv("SETUP_EVENT_ADMIN_PASSWORD"),
            verified=True,
            type='admin'
        )

        db.session.add(user)
        db.session.commit()
        
        # Check if email system is enabled
        if SETUP_EMAIL_ENABLED == "true":
            # Validate impossible email configurations
            if SETUP_ACCOUNT_TYPE == "sso":
                sys.stderr.write(error_prefix + "value of ENV variable \"SETUP_EMAIL_ENABLED\" cannot be true!" + error_suffix)
                mustExit = True
            if mustExit:
                sys.exit(1)
            # Validate empty email configurations
            if not EMAIL_SERVER:
                sys.stderr.write(error_prefix + "value of ENV variable \"EMAIL_SERVER\" cannot be empty!" + error_suffix)
                mustExit = True
            if not EMAIL_PORT:
                sys.stderr.write(error_prefix + "value of ENV variable \"EMAIL_PORT\" cannot be empty!" + error_suffix)
                mustExit = True
            if not EMAIL_PROTOCOL:
                sys.stderr.write(error_prefix + "value of ENV variable \"EMAIL_PROTOCOL\" cannot be empty!" + error_suffix)
                mustExit = True
            if not EMAIL_ADDRESS:
                sys.stderr.write(error_prefix + "value of ENV variable \"EMAIL_ADDRESS\" cannot be empty!" + error_suffix)
                mustExit = True
            if not EMAIL_USERNAME:
                sys.stderr.write(error_prefix + "value of ENV variable \"EMAIL_USERNAME\" cannot be empty!" + error_suffix)
                mustExit = True
            if not EMAIL_PASSWORD:
                sys.stderr.write(error_prefix + "value of ENV variable \"EMAIL_PASSWORD\" cannot be empty!" + error_suffix)
                mustExit = True
            if mustExit:
                sys.exit(1)
            # Validate invalid email configurations
            if EMAIL_PORT and not EMAIL_PORT.isdigit():
                sys.stderr.write(error_prefix + "value of ENV variable \"EMAIL_PORT\" cannot be invalid!" + error_suffix)
                mustExit = True
            if EMAIL_PROTOCOL and EMAIL_PROTOCOL != "ssl" and EMAIL_PROTOCOL != "tls":
                sys.stderr.write(error_prefix + "value of ENV variable \"EMAIL_PROTOCOL\" cannot be invalid!" + error_suffix)
                mustExit = True
            if EMAIL_ADDRESS and not CTFd.utils.validators.validate_email(EMAIL_ADDRESS):
                sys.stderr.write(error_prefix + "value of ENV variable \"EMAIL_ADDRESS\"! cannot be invalid" + error_suffix)
                mustExit = True
            if mustExit:
                sys.exit(1)
            # Email configuration
            CTFd.utils.set_config("mail_server", EMAIL_SERVER)
            CTFd.utils.set_config("mail_port", EMAIL_PORT)
            CTFd.utils.set_config("mail_ssl", EMAIL_PROTOCOL == "ssl")
            CTFd.utils.set_config("mail_tls", EMAIL_PROTOCOL == "tls")
            CTFd.utils.set_config("mailfrom_addr", EMAIL_ADDRESS)
            CTFd.utils.set_config("mail_username", EMAIL_USERNAME)
            CTFd.utils.set_config("mail_password", EMAIL_PASSWORD)
            CTFd.utils.set_config("mail_useauth", True)
            # Email subjects
            CTFd.utils.set_config("verification_email_subject", "Verify your account")
            CTFd.utils.set_config("successful_registration_email_subject", "Account verification confirmation")
            CTFd.utils.set_config("password_reset_subject", "Reset your password")
            CTFd.utils.set_config("password_change_alert_subject", "Password reset confirmation")
            # Email bodies
            CTFd.utils.set_config("verification_email_body",
                "Click the following URL to verify your account:"
                "\n"
                "{url}"
                "\n\n"
                "If the URL is not clickable, try copying and pasting it into your browser."
            )
            CTFd.utils.set_config("successful_registration_email_body",
                "You have successfully registered your account!"
            )
            CTFd.utils.set_config("password_reset_body",
                "Click the following URL to reset your password:"
                "\n"
                "{url}"
                "\n\n"
                "If the URL is not clickable, try copying and pasting it into your browser."
                "\n\n"
                "WARNING: did you not request a password reset? Ignore this email!"
            )
            CTFd.utils.set_config("password_change_alert_body",
                "Your password has been successfully changed!"
                "\n\n"
                "WARNING: did you not request a password reset? Click the following URL to reset your password:"
                "\n"
                "{url}"
                "\n\n"
                "If the URL is not clickable, try copying and pasting it into your browser."
            )
        # Setup is finished
        CTFd.utils.set_config("setup", True)
        # Close database session
        CTFd.models.db.session.close()
        # Clear cache
        with app.app_context():
            CTFd.cache.cache.clear()