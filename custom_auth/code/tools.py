def validate_strong_password(password):
    errors = []
    if len(password) < 8:
        errors.append("Password must be at least 8 characters!")
    if len(password) > 128:
        errors.append("Password must be less than 128 characters!")
    if not any(c.islower() for c in password):
        errors.append("Password must contain at least 1 lowercase character!")
    if not any(c.isupper() for c in password):
        errors.append("Password must contain at least 1 uppercase character!")
    if not any(c.isdigit() for c in password):
        errors.append("Password must contain at least 1 digit character!")
    if not any(c in ",<.>/?;:'\"[{]}-_=+`~!@#$%^&*()" for c in password):
        errors.append("Password must contain at least 1 special character!")
    return errors