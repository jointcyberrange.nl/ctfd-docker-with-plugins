async function ensureJquery() {
    window.jQuery || await CTFd.utils.ajax.getScript("https://code.jquery.com/jquery-3.6.4.min.js")
}

if (typeof window.initialize === 'undefined') {

	window.initialize = async function () {
	    await ensureJquery();

		var challenge_has_backend = CTFd.lib.$("input[name=modal_has_backend]").val()
		if (challenge_has_backend !== 'true') {
			return;
		}

	    var challenge_id = CTFd.lib.$("input[name=modal_challenge_id]").val()
		var user_id = CTFd.lib.$("input[name=modal_user_id]").val()
		var nonce = CTFd.lib.$("input[name=modal_session_nonce]").val()

	    var deployDiv = jQuery("<div id='deployment'>")
	    deployDiv.addClass("chall-deployment mb-3 text-center")
	    deployDiv.insertBefore(".modal-content .challenge-desc")
	    deployDiv.html('<div class="text-center"><i class="fas fa-circle-notch fa-spin fa-1x"></i></div>');
	    
	    var challenge_id = CTFd.lib.$("input[name=modal_challenge_id]").val()
    	    var user_id = CTFd.lib.$("input[name=modal_user_id]").val()
    	    var nonce = CTFd.lib.$("input[name=modal_session_nonce]").val()
    	    
	    headers = { "CSRF-Token": nonce, 'Content-Type': 'application/json' };
	    body = { "challenge_id": challenge_id, "user_id": user_id };

	    jQuery.ajax({
		type: 'GET',
		url: '/container_challenges/namespace?challenge_id=' + challenge_id + '&user_id=' + user_id,
		headers: headers,
		success: function (result) {
		    found = result["found"]
		    
		    if (found === "true") {                
		        jQuery.ajax({
		            type: 'GET',
		            url: '/container_challenges/info?challenge_id=' + challenge_id + '&user_id=' + user_id,
		            headers: headers,
		            dataType: 'json',
		            data: JSON.stringify(body),
		            success: function (result) {
		                jQuery("#deployment").html('<div><button onclick="stop_challenge(' + challenge_id + ',' + user_id + ',' + '\'' + nonce + '\'' +')" class="btn btn-outline-secondary"><i class="fas fa-stop"></i> Stop Challenge</button><div><div class="table-responsive" id="deployinfo"></div>');
		                show_info(result);
		                return;
		            },
		            error: function (result) {
		                jQuery('#deployment').html('<div class="text-center">Cannot extract information from challenge container!</div>');
		                return;
		            }
		        });
		    }
		    if (found === "false") {
		        jQuery("#deployment").html('<div><button onclick="start_challenge(' + challenge_id + ',' + user_id + ',' + '\'' + nonce + '\'' +')" class="btn btn-outline-secondary"><i class="fas fa-play"></i> Start Challenge</button><div><div class="table-responsive" id="deployinfo"></div>');
		    }
		},
		error: function (result) {
		    jQuery('#deployment').html('<div class="text-center">Cannot retrieve challenge status! This might have to do with your Kubernetes.</div>');
		}
	    });
	}

}

if (typeof window.start_challenge === 'undefined') {

	window.start_challenge = function(challenge_id, user_id, nonce) {

	    jQuery('#deployment').html('<div class="text-center"><i class="fas fa-circle-notch fa-spin fa-1x"></i></div>');

	    headers = { "CSRF-Token": nonce, 'Content-Type': 'application/json' };
	    body = { "challenge_id": challenge_id, "user_id": user_id };

	    jQuery.ajax({
		type: 'POST',
		url: '/container_challenges/namespace',
		headers: headers,
		dataType: 'json',
		data: JSON.stringify(body),
		success: function (result) {
		    jQuery.ajax({
		        type: 'POST',
		        url: '/container_challenges/rbac',
		        headers: headers,
		        dataType: 'json',
		        data: JSON.stringify(body),
		        success: function (result) {
		            body["role_name"] = result["role_name"];
		            jQuery.ajax({
		                type: 'POST',
		                url: '/container_challenges/job',
		                headers: headers,
		                dataType: 'json',
		                data: JSON.stringify(body),
		                success: function (result) {
		                    jQuery.ajax({
		                        type: 'GET',
		                        url: '/container_challenges/info?challenge_id=' + challenge_id + '&user_id=' + user_id,
		                        headers: headers,
		                        dataType: 'json',
		                        data: JSON.stringify(body),
		                        success: function (result) {
		                            jQuery("#deployment").html('<div><button onclick="stop_challenge(' + challenge_id + ',' + user_id + ',' + '\'' + nonce + '\'' +')" class="btn btn-outline-secondary"><i class="fas fa-stop"></i> Stop Challenge</button><div><div class="table-responsive" id="deployinfo"></div>');
		                            show_info(result);
		                            return;
		                        },
		                        error: function (result) {
		                            jQuery('#deployment').html('<div class="text-center">Cannot extract information from challenge container!</div>');
		                            return;
		                        }
		                    });
		                },
		                error: function (result) {
		                    jQuery('#deployment').html('<div class="text-center">Cannot deploy job in challenge container namespace!</div>');
		                    return;
		                },
		            });
		        },
		        error: function (result) {
		            jQuery('#deployment').html('<div class="text-center">Cannot deploy RBAC in challenge container namespace!</div>');
		            return;
		        },
		    });
		},
		error: function (result) {
		    jQuery('#deployment').html('<div class="text-center">Cannot deploy challenge container namespace!</div>');
		    return;
		}
	    });
	};

}

if (typeof window.stop_challenge === 'undefined') {

	window.stop_challenge = function(challenge_id, user_id, nonce) {

	    if (typeof challenge_id === "number") {
		challenge_id = String(challenge_id);
	    }
	    if (typeof user_id === "number") {
		user_id = String(user_id);
	    }

	    headers = { "CSRF-Token": nonce, 'Content-Type': 'application/json' };
	    body = { "challenge_id": challenge_id, "user_id": user_id };

	    jQuery.ajax({
		type: 'DELETE',
		url: '/container_challenges/namespace',
		headers: headers,
		dataType: 'json',
		data: JSON.stringify(body),
		success: function (result) { jQuery('#deployment').html('<div class="text-center">Challenge container undeployed!</div>'); },
		error: function (result) { jQuery('#deployment').html('<div class="text-center">Cannot undeploy challenge container!</div>'); }
	    });
	};
	
}

if (typeof window.show_info === 'undefined') {

	window.show_info = function(data) {

	    for (let k in data) {
		if (k === "hyperlinks") {
		    for (const service of data[k]) {
		        if (service['url'].includes("docker-desktop")) {
		            service['url'] = service['url'].replace("docker-desktop", "localhost");
		            jQuery("#deployinfo").append('<div class="text-center"><a href="http://' + service["url"] +'"'+ 'target="_blank" class="btn btn-outline-secondary"> Go to: ' + service["name"] + '<br> http://' + service["url"] + '</a></div>');
		        } else {
		            jQuery("#deployinfo").append('<div class="text-center"><a href="http://' + service["url"] +'"'+ 'target="_blank" class="btn btn-outline-secondary"> Go to: ' + service["name"] + '<br> http://' + service["url"] + '</a></div>');
		        }
		    }
		}

		if (k === "nodes") {
		    var div = document.getElementById('deployinfo');
		    var nodes = document.createElement('table');
		    nodes.classList.add("table");
		    nodes.id = "nodestable";

		    var thead = document.createElement('thead');
		    thead.classList.add("thead-light");

		    var tbody = document.createElement('tbody');

		    var tr = document.createElement('tr');
		    var name = document.createElement('th');
		    var nametext = document.createTextNode("Name");
		    name.setAttribute("data-field", "name");
		    name.appendChild(nametext);

		    var addresses = document.createElement('th');
		    var addresstext = document.createTextNode("Addresses");
		    addresses.setAttribute("data-field", "addresses");
		    addresses.appendChild(addresstext);

		    tr.appendChild(name);
		    tr.appendChild(addresses);
		    thead.appendChild(tr);
		    nodes.appendChild(thead);

		    for (const node of data[k]) {
		        var tr = document.createElement('tr');

		        var name = document.createElement('td');
		        var nametext = document.createTextNode(node["name"]);
		        name.appendChild(nametext);

		        var addresses = document.createElement('td');
		        var addresstext = document.createTextNode(JSON.stringify(node["addresses"]));
		        addresses.appendChild(addresstext);

		        tr.appendChild(name);
		        tr.appendChild(addresses);
		        tbody.appendChild(tr);
		    }

		    nodes.appendChild(tbody);
		    div.appendChild(nodes);
		}

		if (k === "services") {
		    var div = document.getElementById('deployinfo');
		    var services = document.createElement('table');
		    services.classList.add("table");
		    services.id = "servicestable";

		    var thead = document.createElement('thead');
		    thead.classList.add("thead-light");

		    var tbody = document.createElement('tbody');

		    var tr = document.createElement('tr');
		    var name = document.createElement('th');
		    var nametext = document.createTextNode("Name");
		    name.setAttribute("data-field", "name");
		    name.appendChild(nametext);

		    var targetPort = document.createElement('th');
		    var targetPortText = document.createTextNode("Port");
		    targetPort.setAttribute("data-field", "ports");
		    targetPort.appendChild(targetPortText);

		    var nodePort = document.createElement('th');
		    var nodePortText = document.createTextNode("nodePort");
		    nodePort.setAttribute("data-field", "ports");
		    nodePort.appendChild(nodePortText);

		    var clusterip = document.createElement('th');
		    var iptext = document.createTextNode("ClusterIP");
		    clusterip.setAttribute("data-field", "clusterIP");
		    clusterip.appendChild(iptext);

		    tr.appendChild(name);
		    tr.appendChild(clusterip);
		    tr.appendChild(targetPort);
		    tr.appendChild(nodePort);
		    thead.appendChild(tr);
		    services.appendChild(thead);

		    for (const service of data[k]) {
		        var tr = document.createElement('tr');

		        var name = document.createElement('td');
		        var nametext = document.createTextNode(service["name"]);
		        name.appendChild(nametext);

		        var targetPortInfo = document.createElement('td');
		        var targetPortInfoText = document.createTextNode(JSON.stringify(service["ports"][0]["port"]));
		        targetPortInfo.appendChild(targetPortInfoText);

		        var nodePortInfo = document.createElement('td');
		        var nodePortInfoText = document.createTextNode(JSON.stringify(service["ports"][0]["nodePort"]));
		        nodePortInfo.appendChild(nodePortInfoText);

		        var clusterip = document.createElement('td');
		        var iptext = document.createTextNode(service["clusterIP"]);
		        clusterip.appendChild(iptext);

		        tr.appendChild(name);
		        tr.appendChild(clusterip);
		        tr.appendChild(targetPortInfo);
		        tr.appendChild(nodePortInfo);
		        tbody.appendChild(tr);
		    }

		    services.appendChild(tbody);
		    div.appendChild(services);
		}
	    }

	    return;
	};

}

if (typeof window.container_override === 'undefined') {
	CTFd._internal.challenge.data = undefined;
	CTFd._internal.challenge.renderer = null;
	CTFd._internal.challenge.preRender = function() {};
	CTFd._internal.challenge.render = null;
	CTFd._internal.challenge.postRender = initialize;
	CTFd._internal.challenge.submit=function(e){var n={},l={challenge_id:parseInt(CTFd.lib.$("#challenge-id").val()),submission:CTFd.lib.$("#challenge-input").val()};return e&&(n.preview=!0),CTFd.api.post_challenge_attempt(n,l).then(function(e){return 429===e.status?e:(e.status,e)})};
} else {
	window.container_override();
}
