import CTFd
import flask

from CTFd.models import db, Solves
from CTFd.utils.modes import get_model
import math

class ContainerChallengeLog(CTFd.models.db.Model):
    __tablename__ = "container_log"
    __table_args__ = (CTFd.models.db.UniqueConstraint("id"), {})

    id = CTFd.models.db.Column(CTFd.models.db.Integer, primary_key=True)
    challenge_id = CTFd.models.db.Column(CTFd.models.db.Integer, index=True)
    user_id = CTFd.models.db.Column(CTFd.models.db.Integer, index=True)
    start_epoch = CTFd.models.db.Column(CTFd.models.db.Integer, index=True)

    def __init__(self, **kwargs):
        super(ContainerChallengeLog, self).__init__(**kwargs)

class ContainerChallengeConfiguration(CTFd.models.db.Model):
    __tablename__ = "container_configuration"
    __table_args__ = (CTFd.models.db.UniqueConstraint("id"), {})

    id = CTFd.models.db.Column(CTFd.models.db.Integer, primary_key=True)
    username = CTFd.models.db.Column(CTFd.models.db.Text, index=True)
    password = CTFd.models.db.Column(CTFd.models.db.Text, index=True)
    registry = CTFd.models.db.Column(CTFd.models.db.Text, index=True)

    def __init__(self, **kwargs):
        super(ContainerChallengeConfiguration, self).__init__(**kwargs)

class ContainerChallenge(CTFd.models.Challenges):
    __mapper_args__ = {"polymorphic_identity": "container"}

    id = CTFd.models.db.Column(CTFd.models.db.Integer, CTFd.models.db.ForeignKey("challenges.id", ondelete="CASCADE"), primary_key=True)
    compose = CTFd.models.db.Column(CTFd.models.db.Text)
    challenge_type = CTFd.models.db.Column(CTFd.models.db.Text)
    initial = CTFd.models.db.Column(db.Integer, default=0)
    minimum = CTFd.models.db.Column(db.Integer, default=0)
    decay = CTFd.models.db.Column(db.Integer, default=0)

    def __init__(self, **kwargs):
        super(ContainerChallenge, self).__init__(**kwargs)
        self.value = kwargs["initial"]

    @property
    def has_backend(self):
        return len(self.compose).strip() > 0

class ContainerChallengeType(CTFd.plugins.challenges.BaseChallenge):
    id = "container"
    name = "container"
    templates = {
        "create": "/plugins/container_challenges/assets/create.html",
        "update": "/plugins/container_challenges/assets/update.html",
        "view": "/plugins/container_challenges/assets/view.html",
    }
    scripts = {
        "create": "/plugins/container_challenges/assets/create.js",
        "update": "/plugins/container_challenges/assets/update.js",
        "view": "/plugins/container_challenges/assets/view.js",
    }
    route = "/plugins/container_challenges/assets/"
    blueprint = flask.Blueprint("container", __name__, template_folder="templates", static_folder="assets")
    challenge_model = ContainerChallenge

    def delete(challenge):
        CTFd.models.Fails.query.filter_by(challenge_id=challenge.id).delete()
        CTFd.models.Solves.query.filter_by(challenge_id=challenge.id).delete()
        CTFd.models.Flags.query.filter_by(challenge_id=challenge.id).delete()
        files = CTFd.models.ChallengeFiles.query.filter_by(challenge_id=challenge.id).all()
        for f in files:
            CTFd.utils.uploads.delete_file(f.id)
        CTFd.models.ChallengeFiles.query.filter_by(challenge_id=challenge.id).delete()
        CTFd.models.Tags.query.filter_by(challenge_id=challenge.id).delete()
        CTFd.models.Hints.query.filter_by(challenge_id=challenge.id).delete()
        CTFd.models.Challenges.query.filter_by(id=challenge.id).delete()
        ContainerChallenge.query.filter_by(id=challenge.id).delete()
        CTFd.models.db.session.commit()

    @classmethod
    def calculate_value(cls, challenge):
        mod = get_model()

        solve_count = (
            Solves.query.join(mod, Solves.account_id == mod.id)
            .filter(
                Solves.challenge_id == challenge.id,
                mod.hidden == False,
                mod.banned == False,
            )
            .count()
        )

        # If the solve count is 0 we shouldn't manipulate the solve count to
        # let the math update back to normal
        if solve_count != 0:
            # We subtract -1 to allow the first solver to get max point value
            solve_count -= 1

        # It is important that this calculation takes into account floats.
        # Hence this file uses from __future__ import division
        p0 = 0.7
        p1 = 0.96
        c0 = -math.atanh(p0)
        c1 = math.atanh(p1)
        a = lambda x: (1 - math.tanh(x)) / 2
        b = lambda x: (a((c1 - c0) * x + c0) - a(c1)) / (a(c0) - a(c1))

        def get_score(rl, rh, maxSolves, solves):
            s = max(1, maxSolves)
            f = lambda x: rl + (rh - rl) * b(x / s)
            return round(max(f(solves), f(s)))
        value = get_score(challenge.minimum, challenge.initial, challenge.decay, solve_count)
        if value < challenge.minimum:
            value = challenge.minimum

        challenge.value = value
        db.session.commit()
        return challenge


    @classmethod
    def read(cls, challenge):
        """
        This method is in used to access the data of a challenge in a format processable by the front end.

        :param challenge:
        :return: Challenge object, data dictionary to be returned to the user
        """
        challenge = ContainerChallenge.query.filter_by(id=challenge.id).first()
        data = {
            "id": challenge.id,
            "name": challenge.name,
            "value": challenge.value,
            "initial": challenge.initial,
            "decay": challenge.decay,
            "minimum": challenge.minimum,
            "description": challenge.description,
            "connection_info": challenge.connection_info,
            "next_id": challenge.next_id,
            "category": challenge.category,
            "state": challenge.state,
            "has_backend": len(challenge.compose.strip()) > 0,
            "max_attempts": challenge.max_attempts,
            "type": challenge.type,
            "type_data": {
                "id": cls.id,
                "name": cls.name,
                "templates": cls.templates,
                "scripts": cls.scripts,
            },
        }
        return data


    @classmethod
    def update(cls, challenge, request):
        """
        This method is used to update the information associated with a challenge. This should be kept strictly to the
        Challenges table and any child tables.

        :param challenge:
        :param request:
        :return:
        """
        data = request.form or request.get_json()

        for attr, value in data.items():
            # We need to set these to floats so that the next operations don't operate on strings
            if attr in ("initial", "minimum", "decay"):
                value = float(value)
            setattr(challenge, attr, value)

        return ContainerChallengeType.calculate_value(challenge)


    @classmethod
    def solve(cls, user, team, challenge, request):
        super().solve(user, team, challenge, request)

        ContainerChallengeType.calculate_value(challenge)
